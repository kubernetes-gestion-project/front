FROM --platform=linux/amd64 node:18

RUN apt-get update && apt-get install -y git

# Répertoire de travail dans le conteneur
WORKDIR /app

# Copiez le package.json et le package-lock.json (le cas échéant)
COPY package*.json ./

# Installez les dépendances
RUN npm install

# Copiez le reste des fichiers de l'application
COPY . .

EXPOSE 5173

# Générez l'application Vue.js (build)
RUN npm run build

# Commande pour démarrer le serveur web (par exemple, serve)
CMD [ "npm", "run", "host" ]